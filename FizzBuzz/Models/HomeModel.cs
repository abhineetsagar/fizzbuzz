﻿namespace FizzBuzz.Models
{
    using System.Collections;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using System.ComponentModel.DataAnnotations;
    public class HomeModel 
    {
        [Required]
        [RegularExpression(@"^0*(?:[1-9][0-9]{0,2}?|1000)$", ErrorMessage = "Values Should be 1-1000")]
        [Display(Name = "Enter the value:")]
        public int Value { get; set; }

      
    }
}