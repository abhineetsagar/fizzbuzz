﻿namespace FizzBuzz.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using FizzBuzz.Models;
    using StyleCop;
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            HomeModel home = new HomeModel();
            return View(home);
        }

        [HttpPost]
        public ActionResult Index(HomeModel model)
        {
            int a = model.Value;
           
            return RedirectToAction("Numbers", "Home", model);
        }

        [HttpGet]
        public ActionResult Numbers(HomeModel model)
        {
            List<HomeModel> modelList = new List<HomeModel>();
            modelList.Add(model);
            return View(modelList);
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}